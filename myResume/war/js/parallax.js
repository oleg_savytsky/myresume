$(document).ready(function() {
	
	$(window).bind('scroll', function(e) {
		parallax();
	});
	
	$('a.myHome').click(function() {
		$('html, body').animate({ scrollTop:0 }, 1000,
			function() {
				parallax();
			});
		return false;
	});
	
		
	$('a.skills').click(function() {
		$('html, body').animate({ scrollTop:$('#skills').offset().top }, 1000,
			function() {
				parallax();
			});
		return false;
	});
	
	$('a.myCod').click(function() {
		$('html, body').animate({ scrollTop:$('#myCod').offset().top }, 1000,
			function() {
				parallax();
			});
		return false;
	});
	
	$('a.experience').click(function() {
		$('html, body').animate({ scrollTop:$('#experience').offset().top }, 1000,
			function() {
				parallax();
			});
		return false;
	});
	
	$('a.education').click(function() {
		$('html, body').animate({ scrollTop:$('#education').offset().top }, 1000,
			function() {
				parallax();
			});
		return false;
	});
	
	$('a.other').click(function() {
		$('html, body').animate({ scrollTop:$('#other').offset().top }, 1000,
			function() {
				parallax();
			});
		return false;
	});
	
	
	
});

function parallax() {
	var scrollPosition = $(window).scrollTop();
	$('#stars').css('top', (0 - (scrollPosition * .1)) + 'px');
	$('#images').css('top', (0 - (scrollPosition * .5)) + 'px');
}